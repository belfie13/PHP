[Manual :: Coding Standards ⎤↗︎](https://pear.php.net/manual/en/standards.php)

# Coding Standards

**Table of Contents**

- [Indenting and Line Length ⎤↗︎](https://pear.php.net/manual/en/standards.indenting.php)
- [Control Structures ⎤↗︎](https://pear.php.net/manual/en/standards.control.php)
- [Function Calls ⎤↗︎](https://pear.php.net/manual/en/standards.funcalls.php)
- [Class Definitions ⎤↗︎](https://pear.php.net/manual/en/standards.classdef.php)
- [Function Definitions ⎤↗︎](https://pear.php.net/manual/en/standards.funcdef.php)
- [Arrays ⎤↗︎](https://pear.php.net/manual/en/standards.arrays.php)
- [Comments ⎤↗︎](https://pear.php.net/manual/en/standards.comments.php)
- [Including Code ⎤↗︎](https://pear.php.net/manual/en/standards.including.php)
- [PHP Code Tags ⎤↗︎](https://pear.php.net/manual/en/standards.tags.php)
- [Header Comment Blocks ⎤↗︎](https://pear.php.net/manual/en/standards.header.php)
- [Using SVN ⎤↗︎](https://pear.php.net/manual/en/standards.svn.php)
- [Example URLs ⎤↗︎](https://pear.php.net/manual/en/standards.exampleurls.php)
- [Naming Conventions ⎤↗︎](https://pear.php.net/manual/en/standards.naming.php)
- [File Formats ⎤↗︎](https://pear.php.net/manual/en/standards.file.php)
- [E_STRICT-compatible code ⎤↗︎](https://pear.php.net/manual/en/standards.e_strict.php)
- [Error Handling Guidelines ⎤↗︎](https://pear.php.net/manual/en/standards.errors.php)
- [Best practices ⎤↗︎](https://pear.php.net/manual/en/standards.bestpractices.php)
- [Sample File (including Docblock Comment standards) ⎤↗︎](https://pear.php.net/manual/en/standards.sample.php)
- [The PEAR toolbox ⎤↗︎](https://pear.php.net/manual/en/standards.toolbox.php)


## Control Structures

```php
<?php

if ((condition1) || (condition2)) {
    action1;
} elseif ((condition3) && (condition4)) {
    action2;
} else {
    defaultaction;
}
?> 
```

```php
<?php

switch (condition) {
case 1:
    action1;
    break;

case 2:
    action2;
    break;

default:
    defaultaction;
    break;
}
?> 
```

Split long if statements onto several lines
```php
<?php

if (($condition1
    || $condition2)
    && $condition3
    && $condition4
) { }
```

The first condition aligned to others.
```php
<?php

if (   $condition1
    || $condition2
    || $condition3
) { }
```

When long enough to be split, try to simplify it, make variables for conditions.
```php
<?php

$is_foo = ($condition1 || $condition2);
$is_bar = ($condition3 && $condtion4);
if ($is_foo && $is_bar) {
    // ....
}
```


## Ternary operators

Split onto lines, keeping `?` and `:` first.
```php
<?php

$a = $condition1 && $condition2
    ? $foo : $bar;

$b = $condition3 && $condition4
    ? $foo_man_this_is_too_long_what_should_i_do
    : $bar;
?>
```


## Function Calls

spaces:
- none between function name, `(`, and first parameter;
- between `,` and each parameter
- none between last parameter, `)`, and `;`.
- either side of `=`.
```php
<?php

$var = foo($bar, $baz, $quux);
```

align related assignments with spaces.
```php
<?php

$short         = foo($bar);
$long_variable = foo($baz);
?>
```

calls to the same function/method may be aligned by parameter name:
```php
<?php

$this->callSomeFunction('param1',     'second',        true);
$this->callSomeFunction('parameter2', 'third',         false);
$this->callSomeFunction('3',          'verrrrrrylong', true);
?>
```
Split function call on several lines
```php
<?php

$this->someObject->subObject->callThisFunctionWithALongName(
    $parameterOne, $parameterTwo,
    $aVeryLongParameterThree
);
?>
```

- Parameters indented from the function call.
- `(` at the end of the function call line
- `)` on its own line at the end.

The same for nested function calls and for arrays.
```php
<?php

$this->someObject->subObject->callThisFunctionWithALongName(
    $this->someOtherFunc(
        $this->someEvenOtherFunc(
            'Help me!',
            array(
                'foo'  => 'bar',
                'spam' => 'eggs',
            ),
            23
        ),
        $this->someEvenOtherFunc()
    ),
    $this->wowowowowow(12)
);
?>
```

fluent calls may be split onto several lines, indented and beginning with `->`
```php
<?php

$someObject->someFunction("some", "parameter")
    ->someOtherFunc(23, 42)
    ->andAThirdFunction();
?>
```


### Alignment of assignments

```php
<?php

$short  = foo($bar);
$longer = foo($baz);
?>
```

The rule can be broken when name is >|< 8 characters than previous var:
```php
<?php

$short = foo($bar);
$thisVariableNameIsVeeeeeeeeeeryLong = foo($baz);
?>
```


### Split long assigments onto several lines

Assigments may be split onto several lines when the character/line limit would be exceeded. The equal sign has to be positioned onto the following line, and indented by 4 characters.
```php
<?php

$GLOBALS['TSFE']->additionalHeaderData[$this->strApplicationName]
    = $this->xajax->getJavascript(t3lib_extMgm::siteRelPath('nr_xajax'));
?>
```


## Class Definitions

Class declarations have their opening brace on a new line:
```php
<?php
class Foo_Bar
{

    //... code goes here

}
?>
```


## Function Definitions

Function declarations follow the "K&R style":
```php
<?php
function fooFunction($arg1, $arg2 = '')
{
    if (condition) {
        statement;
    }
    return $val;
}
?>
```

Arguments with default values go at the end of the argument list. Always attempt to return a meaningful value from a function if one is appropriate. Here is a slightly longer example:
```php
<?php
function connect(&$dsn, $persistent = false)
{
    if (is_array($dsn)) {
        $dsninfo = &$dsn;
    } else {
        $dsninfo = DB::parseDSN($dsn);
    }

    if (!$dsninfo || !$dsninfo['phptype']) {
        return $this->raiseError();
    }

    return true;
}
?>
```
Split function definitions onto several lines

Functions with many parameters may need to be split onto several lines to keep the 80 characters/line limit. The first parameters may be put onto the same line as the function name if there is enough space. Subsequent parameters on following lines are to be indented 4 spaces. The closing parenthesis and the opening brace are to be put onto the next line, on the same indentation level as the "function" keyword.
```php
<?php

function someFunctionWithAVeryLongName($firstParameter = 'something', $secondParameter = 'booooo',
    $third = null, $fourthParameter = false, $fifthParameter = 123.12,
    $sixthParam = true
) {
    //....
?>
```


## Arrays

Assignments in arrays may be aligned. When splitting array definitions onto several lines, the last value may also have a trailing comma. This is valid PHP syntax and helps to keep code diffs minimal:
```php
<?php

$some_array = array(
    'foo'  => 'bar',
    'spam' => 'ham',
);
?>
```


