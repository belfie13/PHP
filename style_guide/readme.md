# B13 PHP Style Guide

The key words __MUST__, __MUST NOT__, __REQUIRED__, __SHALL__, __SHALL NOT__, __SHOULD__, __SHOULD NOT__, __RECOMMENDED__, __MAY__, and __OPTIONAL__ in this document are to be interpreted as described in [RFC 2119](http://www.ietf.org/rfc/rfc2119.txt).

Any features not available in supported versions of PHP _must not_ be referenced and any existing legacy references _shall_ be removed **A**.**S**.**A**.**P**..


## Basics

- files _must_ use only `<?php` or `<?=` tags.
- files _must_ define symbols OR cause side effects
- each class to a file
    - except enums specific to a class
        - where the enum would not be used without first loading the class.

### Quick Reference:

Adapted from [PSR-12]
```php
<?php

/**
 * File DocBlock.
 */

declare(strict_types=1);

namespace Vendor\Package;

use Vendor\Package\{ClassA as A, ClassB, ClassC as C};
use Vendor\Package\SomeNamespace\ClassD as D;
use function Vendor\Package\{functionA, functionB, functionC};
use const Vendor\Package\{ConstantA, ConstantB, ConstantC};


/**
 * Class DocBlock.
 */
class Foo extends Bar implements
    Boz,
    Baz
 {
    /**
    * Method DocBlock.
    */
    public function sampleFunction(int $a, int $b = null): array
     {
         // method body
     }

    /**
    * Method DocBlock.
    */
    final public static function bar()
     {
        // method body
     }
 }
```

### Control structure examples

#### if, if…elseif…else…
```php
   if ($a === $b)
    {
      bar();
    }
   elseif ($a > $b)
    {
      $foo->bar($arg1);
    }
   else
    {
      BazClass::bar($arg2, $arg3);
    }
   
   if:
      // statements
   elseif:
      // statements
   else:
      // statements
   endif;
```

#### while, do…while
```php
   while ($expr)
    {
      // statements
    }

   while ($expr):
      // statements
   endwhile;

   do
    {
      // statements
    }
   while ($expr)
    {
      // statements
    }
```

#### for
```php   
   for ($expr1; $expr2; $expr3)
    {
      // statements
    }

   for ($expr1; $expr2; $expr3):
      // statements
   endfor;
```

#### foreach
```php
   foreach ($iterable_expr as $value)
    {
      // statements
    }

   foreach ($arr as $key => &$value)
    {
      // reference to $value makes:
      $value = true;
      // an alias of:
      $arr[$key] = true;
    }
   unset($value);

   foreach ($iterable_expr as $key => $value)
    {
      // statements
    }

   foreach ($iterable_expr as list($a, $b))
    {
      // statements
    }

   foreach ($iterable_expr as $value): // ??? i guess, not found in manual ???
      // statements
   endforeach;
```

#### switch
```php
   switch ($expr)
    {
      case 0:
         // stmts
         break;
      case 1:
      case 2:
         // stmts
         break;
      default:
         // stmts
    }

   switch ($expr):
   case 1:
      // statements
   endswitch;
```

#### match
```php
   $ret_val = match ($expr)
    {
      $condition1 => true,
      $condition2, $condition3 => false,
      default => null
    };
```


## Naming

- namespaces nested in associated directories
- class names in `StudlyCaps`.
- constant names in `UPPER_SNAKE_CASE`.
- function names in `camelCase`.
- enum case names in `StudlyCaps`.


### Naming conventions

- __`snake_case`__
- __camelCase__
- __UPPER_SNAKE_CASE__
- __StudlyCaps__


## Optimizations


### `for` Loops

- `$expr1` is *evaluated once* at the **start**.
- `$expr2` is *evaluated* at the **start** of *each* iteration.
    - if `true`: nested statements are *executed* and loop *continues*.
    - if `false`: loop *execution* ends
- `$expr3` is *evaluated* at the **end** of *each* iteration.
- expressions can be separated by `,`.

Examples
```php
for ($i = 1, $j = 0; $i <= 10; $j*= $i, echo $i, $i++);

for ($i = 0, $count = count($it); $i < $count; $i++);
```


# References

[PSR-1: Basic Coding Standard](https://www.php-fig.org/psr/psr-1/)
[PSR-12: Extended Coding Style](https://www.php-fig.org/psr/psr-12/)
