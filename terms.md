###### TODO

Disjunctive Normal Form (DNF) Types
Readonly classes
stand-alone types
dynamic properties
Enumerations
Readonly Properties
First-class Callable
Pure Intersection Types
Never return type
Final class constants
string-keyed arrays
Array unpacking
Named arguments
Attributes
Constructor property promotion
Union types
Nullsafe operator


# PHP Terms

- modifiers
    - visibility <span id="«vis_mod»">«vis_mod»</span>
        - <span id="private">`private`</span>
        - <span id="protected">`protected`</span>
        - <span id="public">`public`</span>
    - extension <span id="«ext_mod»">«ext_mod»</span>
        - <span id="abstract">`abstract`</span>
        - <span id="final">`final`</span>
    - <span id="readonly">`readonly`</span>
    - <span id="static">`static`</span>
- <span id="class">`class`</span>
    - [«ext_mod»](#«ext_mod»)
    - `readonly`
    - <span id="extends">`extends`</span> «class»
    - <span id="implements">`implements`</span> «interface»
    - <span id="use">`use`</span> «trait»
    - members
        - <span id="property">property</span>
            - `const`
            - variable
                - typed
                - `static`
                - `readonly`
                - [«vis_mod»](#«vis_mod»)
        - <span id="method">method</span>
            - `function`
- `interface`
- `enum`
    - `case`
- `const`
- `function`

general
- statement
- expressions
- literal
- constant
    - constant expressions
- static values
- property defaults
- static variable defaults
- parameter defaults
- global constant values
- class constant values
- Function calls
- method calls
- property access
- type check (class, Interface, enum, case)
- function
    - argument list (parameters)
        - type hint
        - initialized (default value / optional)
        - required
    - return type
- class
    - abstract
    - final
    - readonly
    - extends a parent class
    - implements interfaces
    - use traits
    - method (class function)
        - visibility (`public`, `protected`, `private`)
        - static
        - abstract
        - final
        - magic
        - override mehod from parent class
        - access overridden methods `parent::method()`
    - property
        - constant
            - visibility (`public`, `protected`, `private`)
        - variable
            - initialized (default value)
            - typed
            - readonly
            - static
            - visibility (public, protected, private)

## classes

## interfaces

## traits

## Enumerations

defines a type with a set number of possible values.
- Enum is a class
- cases are single-instance objects
- `$name`
- backed enums (int or string)
    - `$value`
    - from(int|string): self
    - tryFrom(int|string): ?self
- methods
- implement interfaces

`const Bar = Direction::Down;`
