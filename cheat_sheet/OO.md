`namespace`
`include` `require` `include_once` `require_once`
`use` `use as`
`trait`, `interface`, `enum`, `class`
`const`, `function`
- `trait`: `use`
- `interface`: `extends`
- `enum`: (`int`|`string`) backed
- `class`: `abstract` `final` `readonly` `extends` `implements` `use`
    - property: `const` var
        - typed visibility `readonly`
    - method: `function`
        - visibility `static` return_type
:visibility: `public` `protected` `private`
## Types
:composite_type:
:intersection_type:
:union_type:
:scalar_type: `bool` `int` `float` `string`
:return_type: `null` `bool` `int` `float` `string` class_type `array` `object` `callable` `never` `void`
- relative_class_type: `self` `parent` `static`
- class_type: class_name interface_name enum_name
- value_type: `true` `false`
:`mixed`:
:`iterable`: which corresponds to the union type of object|resource|array|string|float|int|bool|null and Traversable|array

```php
trait TRAITNAME
 {
    use TRAITNAME;

    public function METHODNAME();
    protected function METHODNAME();
    private function METHODNAME();

    function argList($one, string $typed, $default = false, ?int $nullAble = null);

    function returnType(): CLASSNAME;
    function nullAbleReturnType():?int;

    final public function METHODNAME();
    abstract public function METHODNAME();
    public static function METHODNAME();

    $VAR;
    $VARDEFAULT = true;
    CLASSNAME $VARTYPED;
    ?CLASSNAME $VARNULLABLE = null;
 }
interface INTERFACENAME
 {
    public function METHODNAME();
 }
class CLASSNAME { }

/**

 * SUMMARY.
 * 
 * @todo SUMMARY.
 * @parameter TYPE NAME DEFAULT SUMMARY.
 * @author CIIDMike 
 * 
 */
class CLASSNAME extends CLASSNAME implements INTERFACENAME
 {
    public function METHODNAME();
    protected function METHODNAME();
    private function METHODNAME();
    final public function METHODNAME();
 }

class SUBCLASSNAME extends CLASSNAME { }

abstract class ABSTRACTCLASSNAME
 {
    abstract public function METHODNAME();
    abstract protected function METHODNAME();
    abstract private function METHODNAME();
 }

final class FINALCLASSNAME {}

readonly class READONLYCLASSNAME {}
```

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

"<?"
"<?php"
"?>"
"__class__"
"__dir__"
"__file__"
"__function__"
"__halt_compiler"
"__line__"
"__method__"
"__namespace__"
"__trait__"
"abstract"
"array"
"as"
"binary"
"bool"
"boolean"
"break"
"callable"
"case"
"catch"
"class"
"clone"
"const"
"continue"
"declare"
"default"
"die"
"do"
"double"
"echo"
"else"
"elseif"
"empty"
"enddeclare"
"endfor"
"endforeach"
"endif"
"endswitch"
"endwhile"
"eval"
"exit"
"extends"
"final"
"finally"
"float"
"fn"
"for"
"foreach"
"function"
"global"
"goto"
"if"
"implements"
"include"
"include_once"
"instanceof"
"insteadof"
"int"
"integer"
"interface"
"isset"
"list"
"namespace"
"new"
"object"
"print"
"private"
"protected"
"public"
"readonly"
"real"
"require"
"require_once"
"return"
"static"
"string"
"switch"
"throw"
"trait"
"try"
"unset"
"unset"
"use"
"var"
"while"
"yield from"
"yield"