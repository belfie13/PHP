# How to Lazy Load properties

## related
- readonly props are not immutable
    ∴ an object can

## when to lazy load?
- big resource consumers
- value objects that may not have value

## steps
- unset in __construct()
- create in __get()

```php
    class Test
     {
        public string $lazyProp;
        private string $lazyPrivateProp;
        public readonly string $lazyReadonlyProp;
        public function __construct()
         {
            unset($this->lazyProp);
         }
        public function __get(string $name)
         {
            if ($name === 'lazyProp')
                return $this->lazyProp = 'initialized';
            if ($name === 'lazyPrivateProp')
                return $this->lazyPrivateProp ??= 'initialized';
            if ($name === 'lazyReadonlyProp')
                return $this->lazyReadonlyProp = 'initialized';
         }
     }
    $test = new Test();
    echo $test->lazyProp;
```


## Reference
----
[PHP: rfc:typed_properties_v2](https://wiki.php.net/rfc/typed_properties_v2)
> If a typed property is in uninitialized state,
> either because it has not yet been initialized,
> or because it has been explicitly `unset()`,
> then reads from this property will invoke the `__get()` method if it exists,
> consistently with the behavior of ordinary properties.
> 
> This allows for the following lazy initialization pattern:
> ```php
> class Test {
>     public $untyped;
>     public int $typed;
>  
>     public function __construct() {
>         unset($this->untyped);
>         unset($this->typed); // Not strictly necessary, uninitialized by default
>     }
>  
>     public function __get($name) {
>         if ($name === 'untyped') {
>             return $this->untyped = $this->computeValue1();
>         } else if ($name === 'typed') {
>             return $this->typed = $this->computeValue2();
>         } else {
>             throw new Exception("Unknown property \"$name\"");
>         }
>     }
> }
>  
> $test = new Test;
> var_dump($test->typed); // This calls __get()
> var_dump($test->typed); // This doesn't, as property now initialized
> ```

----
[PHP: rfc:readonly_properties_v2](https://wiki.php.net/rfc/readonly_properties_v2)
> unset a readonly property prior to initialization,
> from the scope where the property has been declared.
> Just like with normal typed properties,
> explicitly unsetting the property makes it visible to magic methods.
> In particular, this enables the usual lazy initialization pattern to work:
> ```php
> class Test {
>     public readonly int $prop;
>  
>     public function __construct() {
>         unset($this->prop);
>     }
>  
>     public function __get($name) {
>         if ($name === 'prop') {
>             $this->prop = $this->lazilyComputeProp();
>         }
>         return $this->$name;
>     }
> }
> ```
