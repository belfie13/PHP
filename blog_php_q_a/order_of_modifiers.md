# Order of Modifiers

Does the order of modifiers matter?
Are there any standards that define order?


## The Modifiers in Question

- public, protected, private
- abstract
- final
- readonly
- static


## The standards

[PEAR Coding Standards](http://pear.php.net/manual/en/standards.php)
