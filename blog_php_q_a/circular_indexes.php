<?php

$items = ['a','b','c','d','e','f','g'];
$count = count($items);

foreach ($items as $index => $entry)
 {
    // to get entry @ +2 indexes
    $subj = $items[($index + 2) % $count];
    // to get entry @ -2 indexes, add the count so modulo 
    $subj = $items[($count + $index - 2) % $count];
 }

/**
    MODULO (%) brings the index (i) back within count (x)

    |%%%%%%%%%%|··i
    0----------X+

    |··i
    0----------X+

     0 1 2 3 4 5 6 7 8 9
    [a,b,c,d,e,f,g]· · •

     0 1 2 3 4 5 6
     7 8 9
    [· · •,d,e,f,g]

    If the index is negative, add the count to bring index back within range

    ·········i··|
    -x----------0----------X+
    
    ·········|---------Xi··|
    -x----------0----------X+

*/
