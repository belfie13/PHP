# ArrayAccess - What happens when adding an item without specifying a key

## Example

```php
class MyArray implements ArrayAccess
 {
    private array $items = [];

    //                 ~ ~ ~ > > > ArrayAccess < < < ~ ~ ~                //
    public function offsetExists(mixed $offset): bool
     {
        return isset($this->items[$offset]);
     }
    public function offsetGet(mixed $offset): mixed
     {
        return $this->items[$offset];
     }
    public function offsetSet(mixed $offset, mixed $value): void
     {
        var_dump($offset);
        $this->items[$offset] = $value;
     }
    public function offsetUnset(mixed $offset): void
     {
        unset($this->items[$offset]);
     }
 }

$obj = new MyArray;
$obj[] = 'value for empty key';
```

## Answer

`$offset` is passed as `NULL` and the `$value` is set to the array with key `''`, the empty string.

testes included:
- setting a value for key `0` to enable auto-indexing which did not work both before and after setting a value without a key.

To provide alternate functionality:
- handle `NULL` `$offset` values in `offsetSet()`

```php
class MyArray implements ArrayAccess
 {
    private array $items = [];

    //                 ~ ~ ~ > > > ArrayAccess < < < ~ ~ ~                //
    public function offsetExists(mixed $offset): bool
     {
        return isset($this->items[$offset]);
     }
    public function offsetGet(mixed $offset): mixed
     {
        return $this->items[$offset];
     }
    public function offsetSet(mixed $offset, mixed $value): void
     {
        if (is_null($offset))
            $this->items[] = $value;
        else
            $this->items[$offset] = $value;
     }
    public function offsetUnset(mixed $offset): void
     {
        unset($this->items[$offset]);
     }
 }

$obj = new MyArray;
$obj[] = 'value for empty key';
$obj[] = 'value for empty key 2';
var_dump($obj);
```
