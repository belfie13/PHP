# Type System

:type declaration: can be added to ensure that the value is the specified type at call time. uses: function arguments, return values, class properties

- stand-alone types
    - `true`
    - `null`
    - `false`
- Union Type
- Intersection Type
- Disjoint Normal Form (DNF) type
    - combination of union and intersection types


- Built-in types
    - `null` type
    - Scalar types:
        - `bool` type
        - `int` type
        - `float` type
        - `string` type
    - `array` type
    - `object` type
    - resource type
    - `never` type
    - `void` type
    - Relative class types: `self`, `parent`, and `static`
- Literal types
    - `false`
    - `true`
- User-defined types (aka class-types)
    - Interfaces
    - Classes
    - Enumerations
- callable type
Composite types

- Intersection `&` Intersection of class-types (interfaces and class names).
- Union `|` Union of types.
- DNF `Union1|(Class1&Class2)|Union3` Disjoint Normal Form type.

Type alias
- `mixed` = `object|resource|array|string|float|int|bool|null`
- `iterable` = `Traversable|array`




# Classes

property is a class defined variable
method is a class defined function
- readonly 
