# PHP Variance

TODO:
- covariance
- contravariance
- invariance
- overriding method return types
- overriding method parameter types
- overriding property types
- union types
- intersection types

## Summary

- If `child` extends `parent` then a `client` expecting `parent` will accept `child` as well.
- If `client` passes a _parameter type_ intended for `parent`, `child` must accept it. Contravariant.
- If `client` needs a _return type_ expected from `parent`, `child` must return it. Covariant.
- If `client` uses a _property type_ available on `parent`, `child` must use it. Invariant.


## method types
The _return types_ of overridden methods must be _covariant_, while _parameter types_ are _contravariant_. 
_Class properties_ are always _invariant_, so children cannot change the type definition unless the new 
signature is both a _sub-type_ and _super-type_ of the inherited one.

## intersection Types

method parameters must support overridden signatures.
must accept all types so adding a type to a child method parameter would 


# References

https://www.howtogeek.com/devops/how-the-new-intersection-types-in-php-8-1-give-you-more-flexibility/
