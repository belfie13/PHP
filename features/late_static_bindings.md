Late Static Bindings

reference the called `class` in a context of `static` inheritance

- A "forwarding call" is a `static` one that is introduced by `self::`, `parent::`, `static::`, or, if going up in the class hierarchy, `forward_static_call()`
- "Late binding" comes from the fact that `static::` will not be resolved using the `class` where the method is defined but it will rather be computed using runtime information.
- "static binding" as it can be used for (but is not limited to) `static` method calls. 

**Example:** `static` and `self` return types with `class` extending and overloading methods.
```php
    <?php

        class A {
            public function test(): self { return new self(); }
        }
        class B extends A {
            public function test(): static { return new static(); }
        }
        class C extends B {}
        class D extends A {}
    $a = new A();
    echo $a->test()::class; // A
    $b = new B();
    echo $b->test()::class; // B
    $c = new C();
    echo $c->test()::class; // C
    $d = new D();
    echo $d->test()::class; // A
    //Outputs: ABCA
```