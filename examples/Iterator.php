<?php

class myIterator implements Iterator {
    protected $position = 0;
    protected $array = array(
        "firstelement",
        "secondelement",
        "lastelement",
    );  

    public function __construct() {
        $this->position = 0;
    }

    public function rewind(): void {
        echo __METHOD__.PHP_EOL;
        $this->position = 0;
    }

    public function current(): mixed {
        echo __METHOD__.PHP_EOL;
        return $this->array[$this->position];
    }

    public function key(): mixed {
        echo __METHOD__.PHP_EOL;
        return $this->position;
    }

    public function next(): void {
        echo __METHOD__.PHP_EOL;
        ++$this->position;
    }

    public function valid(): bool {
        echo __METHOD__.PHP_EOL;
        return isset($this->array[$this->position]);
    }
}

$it = new myIterator;

foreach($it as $key => $value) {
    echo '['.$key.' => '.$value.']'."\n";
}

class MyRecursiveIterator extends MyIterator implements RecursiveIterator
{

    public function __construct(array $array) {
        echo __METHOD__.PHP_EOL;
        $this->position = 0;
        $this->array = $array;
    }

    public function getChildren(): ?MyRecursiveIterator
     {
        echo __METHOD__.PHP_EOL;
        if (!is_array($this->array[$this->position]))
            return null;
        return new MyRecursiveIterator($this->array[$this->position]);
     }
     
    public function hasChildren(): bool
     {
        echo __METHOD__.PHP_EOL;
        return is_array($this->array[$this->position]);
     }
}
$arr = [
    "0",
    "1",
    "2" => [
        "1-0",
        "1-1" => [
            "1-1-0",
            "1-1-1",
            "1-1-2"
        ],
        "1-2" => [
            "1-2-0",
            "1-2-1",
            "1-2-2"
        ]
    ],
    "3",
];

$iterate = function (RecursiveIteratorIterator $it)
 {
    foreach($it as $key => $value) {
        if (is_array($value))
            echo "$key\n";
        else
            echo '['.$key.' => '.$value.']'."\n";
    }
 };

$myIt = new MyRecursiveIterator($arr);

$config = [
    'LEAVES_ONLY' => RecursiveIteratorIterator::LEAVES_ONLY,
    'SELF_FIRST'  => RecursiveIteratorIterator::SELF_FIRST,
    'CHILD_FIRST' => RecursiveIteratorIterator::CHILD_FIRST,
];

foreach ($config as $type => $mode)
 {
     echo PHP_EOL.$type.PHP_EOL;
     $it = new RecursiveIteratorIterator($myIt, $mode);
     $iterate($it);
 }

/* Expected Output

LEAVES_ONLY
myIterator::rewind
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[0 => 0]
myIterator::next
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[1 => 1]
myIterator::next
myIterator::valid
MyRecursiveIterator::hasChildren
MyRecursiveIterator::getChildren
MyRecursiveIterator::__construct
myIterator::rewind
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[0 => 1-0]
myIterator::next
myIterator::valid
myIterator::next
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[3 => 3]
myIterator::next
myIterator::valid
myIterator::valid

SELF_FIRST
myIterator::rewind
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[0 => 0]
myIterator::next
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[1 => 1]
myIterator::next
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
2
MyRecursiveIterator::getChildren
MyRecursiveIterator::__construct
myIterator::rewind
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[0 => 1-0]
myIterator::next
myIterator::valid
myIterator::next
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[3 => 3]
myIterator::next
myIterator::valid
myIterator::valid

CHILD_FIRST
myIterator::rewind
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[0 => 0]
myIterator::next
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[1 => 1]
myIterator::next
myIterator::valid
MyRecursiveIterator::hasChildren
MyRecursiveIterator::getChildren
MyRecursiveIterator::__construct
myIterator::rewind
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[0 => 1-0]
myIterator::next
myIterator::valid
myIterator::valid
myIterator::current
myIterator::key
2
myIterator::next
myIterator::valid
MyRecursiveIterator::hasChildren
myIterator::valid
myIterator::current
myIterator::key
[3 => 3]
myIterator::next
myIterator::valid
myIterator::valid
*/
