<?php

$dir = __DIR__;

function scan($dir)
 {
    $scandir = scandir($dir);
    $ret = [];
    foreach($scandir as $filename)
     {
        if (str_starts_with($filename, '.'))
            continue;
        if (is_dir($filename))
         {
            $ret[$filename] = scan($filename);
         }
        else
            $ret[] = $filename;
     }
    return $ret;
 }

print_r(scan($dir));