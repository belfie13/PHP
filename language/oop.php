<?php

// Interfaces

interface MyInterface { // declare an interface
    public function myInterfaceFunc();
}

// Traits

trait MyTrait { // declare a trait
    private $prop;
}

// Enums

enum MyEnum { // declare an enumeration
    case MyFirstCase;
}

enum MyIntBackedEnum: int { // declare an int backed enum
    case MyFirstIntBackedCase = 1;
}

enum MyStringBackedEnum: string { // declare a string backed enum
    case MyFirstStringBackedCase = '1';
}

// Classes

class MyClass { // declare a class
    public function myClassFunc();
}

class MyExtendedClass extends MyClass { // extend a class
    public function myClassFunc(); // override parent func
}

# final
# abstract
# readonly
