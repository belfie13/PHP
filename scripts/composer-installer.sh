#!/usr/bin/env sh
# Installation of __Composer__ to manage project dependancies
# Assumes this is located in a subdirectory of your project where you want composer installed. ie: `./../bin/composer`

# verify download by checking file signature
EXPECTED_SIGNATURE="$(curl https://composer.github.io/installer.sig)"
# download the installer
/usr/bin/env php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
# get the installers signature, its sha384 hash
ACTUAL_SIGNATURE="$(/usr/bin/env php -r "echo hash_file('sha384', 'composer-setup.php');")"
# report error and exit
if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi
# install Composer to a specific directory with `--install-dir` option 
# (re)name the binary with `--filename` option.
#php composer-setup.php --install-dir="$(php -r "echo __DIR__;")" --filename=composer
/usr/bin/env php composer-setup.php --install-dir=./../bin --filename=composer
RESULT=$?
# remove the installation file
rm composer-setup.php
exit $RESULT
