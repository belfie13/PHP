#!/usr/bin/env sh
## run this script anywhere to update the projects local dependancies.

#export PATH=/opt/local/bin:/usr/local/bin:/usr/bin:/bin:/opt/local/sbin:/usr/sbin:/sbin
cd ..
./bin/composer validate -v
# test version
./bin/composer -V -v
# update composer itself to the latest version.
./bin/composer self-update --2 -v
./bin/composer -V -v
# list locally available platform packages
./bin/composer show --platform -v
# --root-reqs: Restricts the update to your first degree dependencies.
# ./bin/composer --working-dir=./ update --root-reqs -v
./bin/composer --working-dir=./ update -v --prefer-source
# list dependencies as a tree.
./bin/composer show --tree -v
