# Summary

* [Introduction](README.md)

```mermaid
graph TD;
  Node-->Text;
  Text-->Comment;
  Node-->Element;
  Node-->ParentNode;
  Text-->LeafNode;
  Node-->ChildNode;
```

```mermaid
classDiagram
    Node <|-- Text
    Node <|-- Element
    Text <|-- Comment
    class Node{
        +?iterable $childNodes
        +hasChildNodes()
        +addChildNode(Node $child)
        +addChildNodes(Node ...$childNodes)
        +getChildNodes()
        +getIterator()
        +__construct(Node ...$childNodes)
        +__toString()
        +__clone()
        +__call(string $name, array $arguments)
    }
    Node : +str age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
      +String beakColor
      +swim()
      +quack()
    }
    class Fish{
      -int sizeInFeet
      -canEat()
    }
    class Zebra{
      +bool is_wild
      +run()
    }
```